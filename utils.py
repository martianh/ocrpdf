import os
import zipfile
import glob
from flask import render_template


def password_prompt(message):
    return render_template("auth.html", message=message)


def job_done(job, connection, result, *args, **kwargs):
    # NB: this prints to RQ console not flask!
    print(f"job {job} is the job")
    meta = job.get_meta()
    print(f"job meta: {meta}")
    # and we can't just call a redirect from here
    # so will use AJAX polling:
    return


def remove_files(session_folder):
    try:
        files = glob.glob(session_folder + "/*")
        print(f"trying to remove files {files}")
        if files != []:
            for f in files:
                os.remove(f)
                continue
        # remove session dir:
        if os.path.exists(session_folder):
            os.rmdir(session_folder)
        print(f"files {files} removed")
    except Exception as error:
        # FIXME: app not avail here and can't be imported from views.py!
        # app.logger.error("Error removing file(s)", error)
        print(f"Error: {error}")


def zipfiles(session_folder, with_text=False):
    # go get files with no path in our zipfile:
    os.chdir(session_folder)
    zfiles = glob.glob("*-ocr.pdf")

    if with_text is True:
        zfiles.extend(glob.glob("*.txt"))

    zipz = "ocrfiles.zip"
    print(f"{zfiles} are the files to zip")

    with zipfile.ZipFile(zipz, "w") as zipMe:
        for f in zfiles:
            zipMe.write(f)
            print(f"{f} added to zip file {zipz}")
    os.chdir("..")
    return zipz
