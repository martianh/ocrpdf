from flask import Flask
from flask_session import Session
from flask_dropzone import Dropzone

UPLOAD_FOLDER = "uploads/"


def create_app():
    # app and config
    app = Flask(__name__, static_folder="static", template_folder="templates")
    app.config["UPLOAD_FOLDER"] = UPLOAD_FOLDER
    app.config["MAX_CONTENT-LENGTH"] = 1024 * 1024 * 1024  # 1GB in bytes
    app.secret_key = b"yMzpRPvT{G<;#kBR"

    # dropzone config:
    dropzone = Dropzone(app)

    app.config.update(
        DROPZONE_MAX_FILE_SIZE=1024,  # 1GB in MB
        DROPZONE_TIMEOUT=20 * 60 * 1000,  # 20 minutes
        DROPZONE_SERVE_LOCAL=True,
        DROPZONE_MAX_FILES=1,
        DROPZONE_MAX_FILE_EXCEED="You can't upload any more files for now",
        DROPZONE_ALLOWED_FILE_CUSTOM=True,
        DROPZONE_ALLOWED_FILE_TYPE="image/*, .pdf",
        # DROPZONE_REDIRECT_VIEW = 'processing',
        # DROPZONE_UPLOAD_ON_CLICK=True
        # DROPZONE_PARALLEL_UPLOADS=3,
        # DROPZONE_UPLOAD_MULTIPLE=True
    )

    # Session: ends on browser close
    app.config["SESSION_PERMANENT"] = False
    app.config["SESSION_TYPE"] = "filesystem"
    Session(app)

    # flask application:
    return app
