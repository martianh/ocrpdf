import os
import subprocess
from rq import get_current_job

from utils import zipfiles


def do_process_file(filename, output_file, source_lang, session_folder,
                    save_text, double_page):
    infile = session_folder + filename
    outfile = session_folder + output_file
    cmd = ["ocrmypdf",
           # "-v", "1",
           "-l", source_lang,
           "--deskew",
           "--force-ocr",
           "--rotate-pages",
           "--clean",
           "--output-type", "pdf"]

    if save_text is True:
        txtfile = session_folder + (os.path.splitext(filename)[0]) + ".txt"
        sidecar = ["--sidecar", txtfile]
        cmd.extend(sidecar)

    if double_page is True:
        double = ["--unpaper-args", "'--layout' 'double'"]
        cmd.extend(double)

    files = [infile, outfile]
    cmd.extend(files)

    proc = subprocess.Popen(cmd,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.STDOUT,
                            text=True)

    job = get_current_job()
    job.meta['command'] = cmd
    job.meta['stdout'] = 'init'
    job.meta['returncode'] = 'init'
    job.save_meta()

    while proc.poll() is None:
        outline = proc.communicate()[0]
        if outline:
            job.meta['stdout'] = outline
            job.meta['returncode'] = proc.returncode
            job.save_meta()

    # try to mandate waiting for process finished:
    if proc.poll() is not None:
        zipfiles(session_folder, save_text)

    return outfile


def do_process_multifile(input_file_list, source_lang, session_folder):
    output_file_list_path = []  # no leftovers from prev downloads
    for filename in input_file_list:
        print(f"processing {filename}")
        filename_with_path = session_folder + filename
        output_file = (os.path.splitext(filename)[0]) + "-ocr.pdf"
        output_file_with_path = session_folder + output_file

        if os.path.isfile(filename_with_path):
            do_process_file(filename, output_file, source_lang, session_folder)

            output_file_list_path.append(output_file_with_path)

    zipfiles(session_folder)


def tess_langs_list():
    result = subprocess.run(['tesseract', '--list-langs'],
                            check=True, capture_output=True)
    stdout = str(result.stdout)
    langs = stdout.split("\\n")
    langs.pop(0)  # remove first item
    langs.pop()  # remove last item
    langs.pop(langs.index('osd'))  # remove 'osd'
    print(langs)
    return langs


def tess_langs_dict():
    tess_list = tess_langs_list()
    langs_dict = {key: value for key,
                  value in tess_langs.items() if key in tess_list}
    return langs_dict

tess_langs = {
    "afr": "Afrikaans",
    "amh": "Amharic",
    "ara": "Arabic",
    "asm": "Assamese",
    "aze": "Azerbaijani",
    "aze_cyrl": "Azerbaijani - Cyrilic",
    "bel": "Belarusian",
    "ben": "Bengali",
    "bod": "Tibetan",
    "bos": "Bosnian",
    "bre": "Breton",
    "bul": "Bulgarian",
    "cat": "Catalan; Valencian",
    "ceb": "Cebuano",
    "ces": "Czech",
    "chi_sim": "Chinese - Simplified",
    "chi_tra": "Chinese - Traditional",
    "chr": "Cherokee",
    "cos": "Corsican",
    "cym": "Welsh",
    "dan": "Danish",
    "dan_frak": "Danish - Fraktur (contrib)",
    "deu": "German",
    "deu_frak": "German - Fraktur (contrib)",
    "dzo": "Dzongkha",
    "ell": "Greek, Modern (1453-)",
    "eng": "English",
    "enm": "English, Middle (1100-1500)",
    "epo": "Esperanto",
    "equ": "Math / equation detection module",
    "est": "Estonian",
    "eus": "Basque",
    "fao": "Faroese",
    "fas": "Persian",
    "fil": "Filipino (old - Tagalog)",
    "fin": "Finnish",
    "fra": "French",
    "frk": "German - Fraktur",
    "frm": "French, Middle (ca.1400-1600)",
    "fry": "Western Frisian",
    "gla": "Scottish Gaelic",
    "gle": "Irish",
    "glg": "Galician",
    "grc": "Greek, Ancient (to 1453) (contrib)",
    "guj": "Gujarati",
    "hat": "Haitian; Haitian Creole",
    "heb": "Hebrew",
    "hin": "Hindi",
    "hrv": "Croatian",
    "hun": "Hungarian",
    "hye": "Armenian",
    "iku": "Inuktitut",
    "ind": "Indonesian",
    "isl": "Icelandic",
    "ita": "Italian",
    "ita_old": "Italian - Old",
    "jav": "Javanese",
    "jpn": "Japanese",
    "kan": "Kannada",
    "kat": "Georgian",
    "kat_old": "Georgian - Old",
    "kaz": "Kazakh",
    "khm": "Central Khmer",
    "kir": "Kirghiz; Kyrgyz",
    "kmr": "Kurmanji (Kurdish - Latin Script)",
    "kor": "Korean",
    "kor_vert": "Korean (vertical)",
    "kur": "Kurdish (Arabic Script)",
    "lao": "Lao",
    "lat": "Latin",
    "lav": "Latvian",
    "lit": "Lithuanian",
    "ltz": "Luxembourgish",
    "mal": "Malayalam",
    "mar": "Marathi",
    "mkd": "Macedonian",
    "mlt": "Maltese",
    "mon": "Mongolian",
    "mri": "Maori",
    "msa": "Malay",
    "mya": "Burmese",
    "nep": "Nepali",
    "nld": "Dutch; Flemish",
    "nor": "Norwegian",
    "oci": "Occitan (post 1500)",
    "ori": "Oriya",
    "osd": "Orientation and script detection module",
    "pan": "Panjabi; Punjabi",
    "pol": "Polish",
    "por": "Portuguese",
    "pus": "Pushto; Pashto",
    "que": "Quechua",
    "ron": "Romanian; Moldavian; Moldovan",
    "rus": "Russian",
    "san": "Sanskrit",
    "sin": "Sinhala; Sinhalese",
    "slk": "Slovak",
    "slk_frak": "Slovak - Fraktur (contrib)",
    "slv": "Slovenian",
    "snd": "Sindhi",
    "spa": "Spanish; Castilian",
    "spa_old": "Spanish; Castilian - Old",
    "sqi": "Albanian",
    "srp": "Serbian",
    "srp_latn": "Serbian - Latin",
    "sun": "Sundanese",
    "swa": "Swahili",
    "swe": "Swedish",
    "syr": "Syriac",
    "tam": "Tamil",
    "tat": "Tatar",
    "tel": "Telugu",
    "tgk": "Tajik",
    "tgl": "Tagalog (new - Filipino)",
    "tha": "Thai",
    "tir": "Tigrinya",
    "ton": "Tonga",
    "tur": "Turkish",
    "uig": "Uighur; Uyghur",
    "ukr": "Ukrainian",
    "urd": "Urdu",
    "uzb": "Uzbek",
    "uzb_cyrl": "Uzbek - Cyrilic",
    "vie": "Vietnamese",
    "yid": "Yiddish",
    "yor": "Yoruba",
}
