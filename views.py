#!/usr/bin/env python3

import os
import uuid

from werkzeug.utils import secure_filename
from flask import (
    request,
    redirect,
    send_file,
    render_template,
    flash,
    session,
    jsonify,
    after_this_request,
)

from rq import Queue
from rq.job import Job
from rq.command import send_stop_job_command
from worker import conn

from utils import remove_files, job_done, password_prompt
from process import do_process_file, do_process_multifile, tess_langs_dict
from factory import create_app


app = create_app()
q = Queue('ocrpdf', connection=conn, default_timeout=5000)

PASSFILE = "password"
ALLOWED_EXTENSIONS = set(["pdf", "jpg", "gif", "png"])
UPLOAD_FOLDER = "uploads/"

filename = ""
filename_with_path = ""
session_upload_folder = ""


def allowed_file(filename):
    return "." in filename and filename.rsplit(".", 1)[1].lower() in ALLOWED_EXTENSIONS


def file_not_ocrd(filename):
    return not filename.endswith('-ocr.pdf')


@app.route("/", methods=["GET", "POST"])
def login():
    print(request.method)
    if request.method == "GET":
        return password_prompt("Password")
    elif request.method == "POST":
        with open(PASSFILE, "r") as pf:
            password = pf.read().replace("\n", "")
            if request.form["password"] != password:
                return password_prompt("Invalid password, try again")
            else:
                session["status"] = "good"
                print("Password accepted")
                flash("Logged in")
                session['uid'] = uuid.uuid4()  # random uid
                print(session.get('uid'))
                return redirect("/upload")

# NB: with dropzone, each single file dropped runs a POST request.


@app.route("/upload", methods=["GET", "POST"])
def upload_file():
    session_folder = UPLOAD_FOLDER + str(session.get('uid')) + "/"

    # auth first:
    if session.get("status") != "good":
        return redirect("/")
    else:
        if request.method == "GET":
            # remove everything on 'start over' from processing
            @after_this_request
            def remove_file(response):
                remove_files(session_folder)
                return response

        if request.method == "POST":
            # zeroing this means each dropzone upload starts with it empty:
            files = []

            if not os.path.exists(session_folder):
                os.mkdir(session_folder)

            if 'X-Requested-With' in request.headers:
                if request.headers['X-Requested-With'] == "XMLHttpRequest":
                    # dropzone files collect, use items() not .getlist("files[]"):
                    for key, f in request.files.items():
                        if key.startswith("file"):
                            files.append(f)
                            continue
            else:
                # non-dropzone files collect:
                files = request.files.getlist("files[]")

            file_count = len(files)
            print(file_count)
            print(f"we will process {files}")

            if len(files) == 1 and files[0].filename == "":
                nofile()
                return redirect(request.url)

            # save them:
            for ufile in files:
                try:
                    filename = secure_filename(ufile.filename)
                    filename_with_path = session_folder + filename
                    if ufile and allowed_file(filename):
                        if os.path.exists(filename_with_path):
                            print("file already uploaded, skipping")
                            flash(f"file {filename} already uploaded.")
                        else:
                            ufile.save(filename_with_path)
                            print("saved file successfully")
                            flash(
                                f"file {filename} uploaded successfully.")
                    else:
                        flash(
                            f"Allowed file types are: {ALLOWED_EXTENSIONS}", "error"
                        )
                        return redirect(request.url)
                except OSError as e:
                    flash(f"Error uploading file: {e}")
            return redirect("/processing")
        return render_template("upload.html", exts=ALLOWED_EXTENSIONS)


@app.route("/processing", methods=["GET", "POST"])
def process_files():
    tess_dict = tess_langs_dict()
    print(tess_dict)

    output_file_list_path = []  # no leftovers from prev downloads
    input_file_list = []  # clear it just in case
    session_folder = UPLOAD_FOLDER + str(session.get('uid')) + "/"

    for (dirs, dirnames, files) in os.walk(session_folder):
        for f in files:
            if allowed_file(f) and file_not_ocrd(f):
                input_file_list.append(f)

    # handle hitting Next under dropzone when no files:
    if request.method == "GET":
        if input_file_list == []:
            nofile()
            return redirect("/upload")

    if request.method == "POST":
        # maybe no longer needed
        if input_file_list == []:
            nofile()
            return redirect("/upload")

        else:
            print(
                f"we will process the following files: {input_file_list}.")
            filename = ""

            # options from the request:
            source_lang = request.form["Source Language"]
            save_text = bool(request.form.get('Save Text'))
            double_page = bool(request.form.get('Double Page'))

            # multifile:
            if len(input_file_list) > 1:
                output_file = "ocrfiles.zip"
                filename = "multiple-files"

                job = q.enqueue(do_process_multifile,
                                args=(input_file_list, source_lang,
                                      session_folder,),
                                result_ttl=5000,  # keep result
                                failure_ttl=5000,  # keep failure
                                ttl=5000,  # keep in queue
                                on_success=job_done)

            else:
                filename = input_file_list[0]
                output_file = (os.path.splitext(filename)[0]) + "-ocr.pdf"
                job = q.enqueue(do_process_file,
                                args=(filename, output_file, source_lang,
                                      session_folder, save_text, double_page),
                                result_ttl=5000,  # keep result
                                failure_ttl=5000,  # keep failure
                                ttl=5000,  # keep in queue
                                on_success=job_done)

            id = job.get_id()
            print(f"Job ID: {id}")

            # if save text, we need to change file called by our download button:
            if save_text is True:
                download_file = "ocrfiles.zip"
            else:
                download_file = output_file

            return render_template("pls-wait.html",
                                   id=id, filename=filename,
                                   outfile=output_file,
                                   downfile=download_file, lang=source_lang,
                                   stxt=save_text, dblpage=double_page)

    return render_template("processing.html", tess_dict=tess_dict)


def nofile():
    print("no filename")
    flash(
        "You need to select a file to upload. Files are removed after processing.",
        "error",
    )
    # return redirect("/upload")

# test if job completed, call with ajax:


@app.route("/status/<job_id>", methods=['GET'])
def get_status(job_id):
    job = Job.fetch(job_id, connection=conn)
    print(job_id)

    if job is None:
        response = {'status': 'unknown'}
    else:
        response = {
            'status': job.get_status(refresh=True),
            'result': job.result,  # return value of do_process_file
            'meta': job.get_meta(refresh=True)
        }
        if job.is_failed:
            response['message'] = job.exc_info.strip().split('\n')[-1]
    return jsonify(response), 200


@app.route("/stop/<job_id>", methods=['GET'])
def stop_job(job_id):
    job = Job.fetch(job_id, connection=conn)

    status = job.get_status(refresh=True)

    if status == "started":
        send_stop_job_command(conn, job_id)
        # delete job from redis:
        job.delete()
        flash(f'Job {job_id} aborted!')
        return redirect("/processing")
    else:
        flash(f"No job started? ({job_id}, status {status})")
        return redirect("/processing")


@app.route("/return-files/<filename>")
def return_files_tut(filename):
    session_folder = UPLOAD_FOLDER + str(session.get('uid')) + "/"
    out_file_path = session_folder + filename

    # remove files afterwards:
    @after_this_request
    def remove_file(response):
        remove_files(session_folder)
        return response

    if os.path.isfile(out_file_path):
        return send_file(out_file_path, as_attachment=True, download_name="")
    else:
        flash("Error, no file. Files are deleted upon processing. Try again.")
        return redirect("/upload")
