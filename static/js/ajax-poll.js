
// (un)fold our CLI command
function toggleCommand() {
    // Get the DOM reference
    let btn = document.getElementById("commandbtn");
    let cmd = document.querySelector(".command");
    // Toggle
    if (cmd.style.display == "block") {
        cmd.style.display = "none";
        btn.innerText = "view command";
    } else {
        cmd.style.display = "block";
        btn.innerText = "hide command";
    }
}

// run on HTML loaded:
document.addEventListener("DOMContentLoaded", function() {

    const jobPara = document.querySelector(".job_id");
    let jobId = jobPara.innerText;
    let statusUrl = "/status/" + jobId;
    const resultText = document.querySelector(".result_text");
    const download = document.getElementById("download");
    const abort = document.getElementById("abort");
    const job = document.querySelector('.job');
    const counter = document.querySelector('.counter');
    let countData = 0;
    const waiting = document.querySelector('.waiting');
    const output = document.getElementById("output");
    const status = document.getElementById('status');
    const command = document.querySelector('.command');

    countConv = time_conv(countData);
    counter.innerText = countConv;

    function time_conv(num) {
        var minutes = Math.floor(num / 60);
        var seconds = num % 60;
        return minutes + ':' + seconds;
    }

    function countDataInc() {
        countData++;
        countConv = time_conv(countData);
        counter.innerText = countConv;
    }

    // our timer:
    if (countData == 0) {
        // stop our loop calling setInterval twice:
        // countData++
        timer = setInterval(countDataInc, 1000);
    };

    function check_job_status(status_url) {

        $.getJSON(status_url, function(data) {
            console.log(data);
            switch (data.status) {
            case "unknown":
                output.style.visibility="visible";
                stdout.innerText = "Unknown Job ID";
                clearInterval(timer);
                break;
            case "finished":
                // cmd succeeded:
                if (data.meta.returncode == 0) {
                    job.style.background="#ACD1AF";
                    download.style.visibility="visible";
                    waiting.style.visibility="hidden";
                    abort.remove();
                    resultText.innerText = "Processing done! Click below to download your file(s).";
                    document.querySelector('h3').innerText = "Processing your file(s)... DONE!";
                    status.innerText = data.status;

                    // timer cleanup:
                    clearInterval(timer);

                    // display output of job command:
                    output.style.visibility="visible";
                    stdout.innerText = data.meta.stdout;

                } else {
                    // job ran but shell cmd failed:
                    job.style.background="#FF7276";
                    output.style.visibility="visible";
                    stdout.innerText = data.meta.stdout;
                    clearInterval(timer);
                }
                break;
            case "failed":
                job.style.background="#FF7276";
                output.style.visibility="visible";
                stdout.innerText = data.message;
                clearInterval(timer);
                break;
            default:
                // output command
                if (data.meta.command) {
                    cmdlist = data.meta.command;
                    command.innerText = data.meta.command.join(" ");
                }

                // our waiting period worm:
                waiting.innerText += '.';

                status.innerText = data.status;
                // live output:
                // output.style.visibility="visible";

                // if (data.meta.output != 'init') {
                //     let lineout = data.meta.stdout
                //     lineout = lineout.replace( /[\r\n]+/gm, "" )
                //     let p = document.createElement('pre')
                //     p.textContent = lineout
                //     if (output.lastChild.textContent != lineout) {
                //     output.appendChild(p);
                //     }
                // }
                // // queued/started/deferred
                setTimeout(function() {
                    check_job_status(status_url);
                }, 500);  // every 1/2 second
            }
        });
    }
    
    // actually call the function on page load:
    check_job_status(statusUrl);
});
